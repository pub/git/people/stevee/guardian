NAME = guardian
VERSION = 2.0.2

PACKAGE_NAME = $(NAME)-$(VERSION)
PACKAGE_VERSION = $(VERSION)

DESTDIR =
PREFIX = /usr
BINDIR = $(PREFIX)/bin
SBINDIR = $(PREFIX)/sbin
SYSCONFDIR = $(PREFIX)/etc

PERL_VER := $(shell eval "$$(perl -V:version)"; echo $${version};)
PERL_SITELIB_DIR := $(shell eval "$$(perl -V:installsitelib)"; echo $${installsitelib};)

PERL_DIR = $(DESTDIR)$(PERL_SITELIB_DIR)/Guardian/

SED = sed

substitutions = \
	'|PACKAGE_NAME=$(PACKAGE_NAME)|' \
	'|PACKAGE_VERSION=$(PACKAGE_VERSION)|'

SED_PROCESS = \
	$(SED) $(subst '|,-e 's|@,$(subst =,\@|,$(subst |',|g',$(substitutions)))) \
		< $< > $@

all: guardian guardianctrl

guardian: guardian.in Makefile
	$(SED_PROCESS)

guardianctrl: guardianctrl.in Makefile
	$(SED_PROCESS)

install:
	# Create directory and copy perl modules
	-mkdir -pv $(PERL_DIR)
	cp -rvf modules/* $(PERL_DIR)
	# Create sbindir and install guardian daemon
	-mkdir -pv $(DESTDIR)/$(SBINDIR)
	install -v -m 755 guardian $(DESTDIR)/$(SBINDIR)
	# Create bindir and install guardianctrl
	-mkdir -pv $(DESTDIR)$(BINDIR)
	install -v -m 755 guardianctrl $(DESTDIR)/$(BINDIR)

clean:
	rm -vf guardian
	rm -vf guardianctrl

dist:
	# Create temporary dist directory.
	-mkdir $(PACKAGE_NAME)
	# Copy all required files to the directory.
	cp -af \
		Makefile \
		*.in \
		modules \
		README \
		COPYING $(PACKAGE_NAME)
	# Create dist tarball.
	tar -czf $(PACKAGE_NAME).tar.gz $(PACKAGE_NAME)
	# Remove temporary directory again.
	rm -rf $(PACKAGE_NAME)
